package a1;
/*
 * author(s):
 * Elke Beispielfrau    (Elke.Beispielfrau@Hamburg-UAS.eu)
 * Otto Mustermann      (Otto.Mustermann@Hamburg-UAS.eu)
 */


// shall fulfill requirements given by "A1"
public class HelloWorld {
    
    // shall print Hello World
    public static void main( String[] args ){
        System.out.printf( "Hello World !" );
    }//method()
    
}//class

